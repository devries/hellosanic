FROM python:3.8-alpine
RUN apk add --update \
    gcc \
    make \
    musl-dev \
    libuv-dev \
  && rm -rf /var/cache/apk/*

RUN python -m pip install pipenv
COPY Pipfile* /app/
RUN cd /app && pipenv install --system

RUN addgroup -g 2000 apprunner
RUN adduser -u 2000 -G apprunner -S apprunner

COPY --chown=apprunner:apprunner main_app.py /app/
COPY --chown=apprunner:apprunner templates /app/templates
COPY --chown=apprunner:apprunner static /app/static
WORKDIR /app

EXPOSE 8080

USER apprunner

CMD ["python","-m","sanic","main_app.app","--host","0.0.0.0","--port","8080","--workers","4"]
