import os
from sanic import Sanic, response
from jinja2 import Environment, PackageLoader, select_autoescape
import socket

app = Sanic(__name__)

template_env = Environment(loader=PackageLoader('main_app','templates'),
        autoescape=select_autoescape(['html', 'xml']),
        enable_async=True)

hostname = socket.gethostname()

@app.route('/')
async def root_index(request):
    template = template_env.get_template('index.html')
    rendered_template = await template.render_async(hostname=hostname)
    return response.html(rendered_template)

@app.route('/json')
async def json_reply(request):
    heads = request.headers

    resp_struct = {'headers':dict(heads),
            'environment':dict(os.environ)}

    return response.json(resp_struct, headers={'Cache-Control': 'no-cache, no-store'})

@app.route('/favicon.ico')
async def favicon_reply(request):
    return await response.file('static/favicon.ico')

if __name__=='__main__':
    app.run(host="0.0.0.0", port=8080, debug=True)
